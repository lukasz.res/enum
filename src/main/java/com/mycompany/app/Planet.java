package com.mycompany.app;

import lombok.Getter;

public enum Planet {
    EARTH("Earth", 5.97219e24, 6.371e6),
    MOON("Moon", 7.34767309e+22, 1.737e6),
    MARS("MARS", 6.41693e+23, 3.393e6),
    JUPITER("Jupiter", 1.89813e+27, 7.1492e6);

    @Getter
    private final String name;
    @Getter
    private final double mass; //kilograms
    @Getter
    private final double radius; //m
    @Getter
    private final double surfaceGravity; //m/s^2

    private static final double G = 6.674E-11; //Gravitational constant m^3 / kg s^2


    Planet(String name, double mass, double radius){
        this.name = name;
        this.mass = mass;
        this.radius = radius;
        this.surfaceGravity = G * mass;
    }

    public double surfaceWeight(double mass){
        return mass * surfaceGravity; // F = ma
    }

    @Override
    public String toString(){
        return "I'm " + this.getClass().getSimpleName() + " ";
    }
}
