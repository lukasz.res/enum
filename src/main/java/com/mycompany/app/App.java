package com.mycompany.app;

public class App {
    public static void main(String[] args) {
        for (Planet planet : Planet.values()) {
            System.out.println(
                    planet + planet.getName() + " (" + planet.name() + "): "
                            + "mass: " + planet.getMass() + " kg. radius " + planet.getRadius() + "m.");
        }

        double earthWeight = (args.length > 0) ? Double.parseDouble(args[0]) : 85;

        double mass = earthWeight / Planet.EARTH.getSurfaceGravity();
        for (Planet planet : Planet.values()) {
            System.out.printf("Weight on %-10s %9.2f kg. %n", planet.getName() + " is"
                    , planet.surfaceWeight(mass));
        }
    }
}
